const HtStudies = {


    // CONSTANTS

    API_LN: ["en", "fr", "de", "es",],
    BROWSER_LN: browser.i18n.getUILanguage().split("-")[0],

    API_URL: "",
    FRONT_URL: "",

    TEAM_ID: null,


    // APPLICATION DATA
    data: {
        callback: {
            url: window.location.protocol + "//" + window.location.hostname + "/",
            hash: "htStudies"
        },

        text: {
            
            colon: browser.i18n.getMessage("colon"),
            
            menuItem: browser.i18n.getMessage("menuItem"),
    
            mainTitle: browser.i18n.getMessage("mainTitle"),
            mainSubtitle: browser.i18n.getMessage("mainSubtitle"),

            description: browser.i18n.getMessage("description"),

            dataLabel: browser.i18n.getMessage("dataLabel"),
            downloadData: browser.i18n.getMessage("downloadData"),
            
            onGoingStudiesTitle: browser.i18n.getMessage("onGoingStudiesTitle"),

            notifySubscriptionOn: browser.i18n.getMessage("notifySubscriptionOn"),
            notifySubscriptionOff: browser.i18n.getMessage("notifySubscriptionOff"),
    
            sidebarTitle: browser.i18n.getMessage("sidebarTitle"),
            sidebarNoUser: browser.i18n.getMessage("sidebarNoUser"),
            sidebarLoginButton: browser.i18n.getMessage("sidebarLoginButton"),
            sidebarUserLabel: browser.i18n.getMessage("sidebarUserLabel"),
            sidebarTeamLabel: browser.i18n.getMessage("sidebarTeamLabel"),
        },

        icons: {
            download_data: browser.runtime.getURL("images/icons/download_data.svg")
        }

    },


    // METHODS

    initAPIUrl: function () {
        // Return language to use with API
        console.log(browser.i18n.getUILanguage());
        let ln = this.API_LN.includes(this.BROWSER_LN) ? this.BROWSER_LN : this.API_LN[0];
        this.API_URL = "https://htstudies.alwaysdata.net" + "/"  + ln;
    },

    isConnected: function () {
        // Check if the user is connected to his account on Hattrick website
        return document.getElementById("teamLinks").querySelector("a") ? true : false;
    },

    isHtStudiesPage: function () {
        // Check if the current page is HTStudies
        return window.location.href === this.FRONT_URL; 
    },

    addHtStudiesLink: function () {
        // add HTStudies entry in the main Hattrick menu
        
        if (this.FRONT_URL != "") {

            let item = document.getElementById("menu").lastElementChild.previousElementSibling;
            let newItem = item.cloneNode(true);
        
            if (newItem.querySelector('div')) {
                newItem.removeChild(newItem.querySelector('div'));
            }
        
            let newLink = newItem.querySelector('a');
            newLink.textContent = this.data.text.menuItem;
            newLink.href = this.FRONT_URL;
        
            document.getElementById("menu").appendChild(newItem);

            return null;
        }
    },

    getTemplatedNode: async function (templatePath) {
        // returned html after handlebars compile the given template

        let tpl_url = browser.runtime.getURL(templatePath);
        
        const query = await fetch(tpl_url);
        const tpl = await query.text();
        let template = Handlebars.compile(tpl);
        let html = template(this.data);
        
        // Convert html to DOM node
        let parser = new DOMParser();
        html = parser.parseFromString(html, "text/html").body.firstChild;
        

        let div = document.createElement("div");
        div.className = "hts";
        div.appendChild(html);

        return div;
    },

    updateData: async function () {
        // Fetch server and update object data
        let query = await fetch(this.API_URL + "/app_data/team_" + this.TEAM_ID, {credentials: 'include'});
        let data = await query.json();

        for (const key in data) {
            this.data[key] = data[key];
        }
    },

    manageSubscriptions: function (event) {
        
        // disable switch input during process
        event.target.disabled = true;
        
        // check if action is supposed to subscribe (switch on) or unsubscribe (switch off)
        // and adapt url query in consequence
        let action = event.target.checked ? "subscribe" : "unsubscribe";
        let url = this.API_URL + "/" + action + "/team_" + this.data.team.id + "/study_" + event.target.value;
        let notify = document.querySelector(".hts-study-head .study-notify");
        
        // fetch the server to manage subscription
        fetch(url, {credentials: 'include'})
            .then(query => query.json())
            .then(data => {
                // if the operation was ok, launch a notification
                if (data.success) {
                    

                    if (data.subscription) {
                        notify.innerText = this.data.text.notifySubscriptionOn;
                        notify.classList.contains("warning") ? 
                            notify.classList.replace("warning", "success")
                            :
                            notify.classList.add("success");
                    }
                    else {
                        notify.innerText = this.data.text.notifySubscriptionOff;
                        notify.classList.contains("success") ? 
                            notify.classList.replace("success", "warning")
                            :
                            notify.classList.add("warning");
                    }
                    
                    // make the notification appearing
                    notify.style.visibility = "visible";
                    notify.classList.add("visible");
                    
                    // and hide it after 5 seconds 
                    setTimeout(() => {
                        notify.classList.remove("visible");
                        event.target.disabled = false;
                    }, 5000);
                }

                // if an error occured, log it
                else {
                    console.log("an error occured when fetching " + url);
                    event.target.disabled = false;
                }
                
            })
            .catch(error => {
                // log in case of error
                console.log(error);
              });
    },

    addSwitchListener: function (node) {
        // Attach manageSubscriptions method to all switchs contained in the given node
        inputs = node.querySelectorAll(".hts-switch input");
        inputs.forEach(input => {
            input.addEventListener(
                "change",
                e => this.manageSubscriptions(e));
        });
    },

    displayHtStudiesPage: function () {
        // method returning the UI

        // main body UI
        // add title to main block and add specific class to hide Hattrick content
        document.getElementById("ctl00_ctl00_CPContent_divStartMain")
                .querySelector("h2 span").innerText = this.data.text.mainTitle + " | " + this.data.text.mainSubtitle;
    
        let mainBody = document.getElementById("mainBody");
        mainBody.classList.add("ht_studies_empty");
    
    
        // sidebar UI
        // add title to the sidebar and add specific class to hide Hattrick content
        let sideBar = document.getElementById("sidebar");
        let sideBarFirstBox = sideBar.firstElementChild;
        sideBarFirstBox.classList.add("hts_hide");
        let sideBarSecondBox = sideBarFirstBox.nextElementSibling;
        sideBarSecondBox.querySelector("h2").innerText = this.data.text.sidebarTitle;
        sideBody = sideBarSecondBox.querySelector(".boxBody");
        sideBody.classList.add("ht_studies_empty");
    

        // Display main body and add event listener to switchs
        this.getTemplatedNode("templates/main.html")
            .then((html) => {mainBody.appendChild(html);})
            .then(() => {this.addSwitchListener(mainBody);});
        
        // Display side body
        this.getTemplatedNode("templates/sidebar.html")
            .then((html) => {sideBody.appendChild(html);});

        return null;
    }

}

let app = HtStudies;

app.initAPIUrl();

if (app.isConnected()) {
    
    // Define team ID and url
    app.TEAM_ID = document.querySelector("#teamLinks a").href.split("TeamID=")[1]
    app.FRONT_URL = window.location.protocol + "//" + window.location.hostname + "/#htStudies";

    app.addHtStudiesLink();

    if (app.isHtStudiesPage()) {
        
        // Fetch data from server and save in app object
        app.updateData().then(() => {app.displayHtStudiesPage();});
    }
}