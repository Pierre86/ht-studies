import hashlib
import importlib
import pathlib
import csv
import os
import inspect
import zipfile

from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils import timezone


def create_salt():
    return os.urandom(16)


class User(AbstractUser):

    objects = UserManager()

    ht_id = models.IntegerField(unique=True)
    token_key = models.CharField(max_length=20)
    token_secret = models.CharField(max_length=20)

    USERNAME_FIELD = "ht_id"

    def __repr__(self):
        return f"<User : {self.username} ({self.ht_id})>"


class Data(models.Model):
    name = models.CharField(max_length=30)


class Study(models.Model):
    name = models.CharField(max_length=100)
    codename = models.CharField(max_length=30, unique=True)
    description = models.TextField()
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(null=True, blank=True)
    active = models.BooleanField(default=False)
    data = models.ManyToManyField(Data,
                                  related_name="studies",
                                  )
    salt = models.BinaryField(default=create_salt)
    data_published = models.BooleanField(default=False)

    @property
    def study_module(self):
        """
        Return study module
        """
        return importlib.import_module(f".models.study_{self.codename}",
                                       "core")

    def run(self):
        return self.study_module.run(self)

    @property
    def study_classes(self):
        """
        Return study classes aimed to be exported
        """
        return [c for (name, c) in inspect.getmembers(self.study_module,
                                                      inspect.isclass)
                if (issubclass(c, StudyModel)
                    and c is not StudyModel
                    and c.csv_content)
                ]

    @property
    def directory(self):
        directory = pathlib.Path(
            settings.BASE_DIR / "csv" / f"study_{self.codename}")
        directory.mkdir(parents=True, exist_ok=True)
        return directory

    @property
    def zip_path(self):
        return self.directory / f'{self.codename}_data.zip'

    def export_to_csv_and_zip(self):
        csv_files = [study_class.export_to_csv(self.directory)
                     for study_class in self.study_classes]

        with zipfile.ZipFile(
                file=self.zip_path,
                mode='w',
                compression=zipfile.ZIP_DEFLATED,
        ) as zip_file:
            # writing each file one by one
            for file in csv_files:
                zip_file.write(filename=file, arcname=file.name)

    def __repr__(self):
        return f"<Study : {self.name} ({self.pk})>"


class Team(models.Model):
    ht_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=50)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="teams",
                             )
    salt = models.BinaryField(default=create_salt)
    studies = models.ManyToManyField(Study,
                                     related_name="teams",
                                     through="Subscription",
                                     )

    def __repr__(self):
        return f"<Team : {self.name} ({self.ht_id})>"


class Subscription(models.Model):
    study = models.ForeignKey(Study,
                              related_name="subscriptions",
                              on_delete=models.CASCADE)
    team = models.ForeignKey(Team,
                             related_name="subscriptions",
                             on_delete=models.CASCADE)
    last_update = models.DateTimeField(null=True, blank=True)


class StudyModel(models.Model):

    class Meta:
        abstract = True

    csv_content = dict()

    @classmethod
    def export_to_csv(cls, directory):

        def recursive_attribute(obj, attr_chain):
            """
            interpret attribute chain string "xxx.yyy.zzz"
            and return final value as a string
            """
            current_attr = attr_chain.split(".")[0]
            following_attr = ".".join(attr_chain.split(".")[1:])

            if not following_attr:
                return str(getattr(obj, current_attr, ""))

            elif getattr(obj, current_attr, None) is None:
                return "None"

            else:
                return recursive_attribute(getattr(obj, current_attr),
                                           following_attr)

        def csv_value(obj, tuple_value):
            """
            return value, eventually hashed with sha256,
            from tuple (attr, "hash")
            """
            if not tuple_value or not isinstance(tuple_value[0], str):
                raise ValueError("tuple_value must be a tuple with a string "
                                 "as the first item")

            else:
                value = recursive_attribute(obj, tuple_value[0])

                # if value is not None and hash is requested
                if (
                        not value == "None"
                        and len(tuple_value) >= 2
                        and tuple_value[1] == "hash"
                ):

                    # if set, fetch a custom salt to hash value
                    custom_salt = None
                    if len(tuple_value) >= 3:
                        custom_salt = getattr(obj, tuple_value[2]).salt

                    # if hash is requested, model must have a team attribute
                    # pointing to a Team model instance, which have a salt
                    codename = cls.__module__.split(".")[-1][len("study_"):]
                    study_salt = Study.objects.get(codename=codename).salt

                    # if set, use custom_salt
                    # else hash only with study_salt
                    value_to_hash = (value.encode() + study_salt + custom_salt
                                     if custom_salt is not None
                                     else value.encode() + study_salt)

                    # value is hashed with the salt
                    value = hashlib.sha256(value_to_hash).hexdigest()

                return value

        # check cls.csv_content
        if not cls.csv_content or not isinstance(cls.csv_content, dict):
            raise ValueError("csv_fields must be a non empty dictionnary")

        # if format is ok, create csv file to the given directory
        else:

            file_path = directory / f"{cls.__name__}.csv"

            with open(file=file_path,
                      mode="w",
                      newline="",
                      ) as csv_file:

                fieldnames = cls.csv_content.keys()
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                writer.writeheader()

                for entry in cls.objects.all():
                    writer.writerow(
                        {key: csv_value(entry, value)
                         for key, value in cls.csv_content.items()}
                        )

            return file_path
