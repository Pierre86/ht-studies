import datetime
from datetime import timedelta
import logging

from django.db import models
from django.conf import settings
from pychpp.chpp import CHPP
from pychpp.ht_datetime import HTDatetime

from ..models import Team, StudyModel


class Match(StudyModel):
    ht_id = models.IntegerField()
    datetime = models.DateTimeField()

    @property
    def date(self):
        return self.datetime.date()


class FormationExperience(StudyModel):

    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    datetime = models.DateTimeField()
    formation = models.CharField(max_length=3)
    level = models.IntegerField()

    csv_content = {
        "team_id_hash": ("team.ht_id", "hash", "team",),
        "date": ("date",),
        "formation": ("formation", "hash",),
        "level": ("level",),
    }

    @property
    def date(self):
        return self.datetime.date()


class FormationUsePeriod(StudyModel):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE,
                              blank=True, null=True)
    formation = models.CharField(max_length=3)
    start_minute = models.IntegerField()
    end_minute = models.IntegerField()
    duration = models.IntegerField()

    csv_content = {
        "team_id_hash": ("team.ht_id", "hash", "team",),
        "match_id_hash": ("match.ht_id", "hash", "team",),
        "match_date": ("match.date",),
        "formation": ("formation", "hash",),
        "duration": ("duration",),
    }


class MatchEvents(StudyModel):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    formation = models.CharField(max_length=3)
    event_code = models.IntegerField()
    minute = models.IntegerField()
    formation_level = models.IntegerField()


def run(study):

    # Get an instance of a logger
    logger = logging.getLogger(__name__)

    now = HTDatetime(timezone="CET")

    formations = ["550", "541", "532", "523", "451",
                  "442", "433", "352", "343", "253",
                  ]

    # Boucle sur les équipes ayant souscrit à l'étude

    # Lundi midi -> Mardi midi | Vendredi 0h -> Vendredi 23h59
    # Récupérer temps joué dans chaque formation lors du dernier match

    # if we are between Monday 12:00 and Tuesday 11:59
    # or if we are Friday
    if (
            (now.weekday == 1 and now.hour >= 12)
            or (now.weekday == 2 and now.hour < 12)
            or now.weekday == 5
    ):

        for team in study.teams.all():

            subscription = team.subscriptions.get(study=study)

            if subscription.last_update is None or (
                    HTDatetime().datetime - subscription.last_update
                    > timedelta(hours=36)
            ):
                try:
                    chpp = CHPP(consumer_key=settings.CHPP_CONSUMER_KEY,
                                consumer_secret=settings.CHPP_CONSUMER_SECRET,
                                access_token_key=team.user.token_key,
                                access_token_secret=team.user.token_secret,
                                )

                    # Get formation experience
                    training = chpp.training(team_ht_id=team.ht_id)

                    for formation in formations:
                        level = getattr(training, f"experience_{formation}",
                                        None)

                        FormationExperience.objects.create(
                            team=team,
                            datetime=now.datetime,
                            formation=formation,
                            level=level,
                        )

                    # Get last played formations

                    if now.weekday in (1, 2,):
                        limit_date = now - timedelta(weeks=1)
                        limit_date.weekday = 5
                        limit_date.hour = 0
                    else:
                        limit_date = HTDatetime.from_datetime(now.datetime)
                        limit_date.weekday = 2
                        limit_date.hour = 12

                    limit_date.minute = 0
                    limit_date.second = 0

                    last_matches = chpp.matches_archive(
                        ht_id=team.ht_id,
                        first_match_date=limit_date,
                        hto=False,
                    )

                    # last_match is None if no match found
                    last_match = last_matches[-1] if len(last_matches) else None

                    # search and store played formation if a match was found
                    if last_match is not None:
                        match, _ = Match.objects.get_or_create(
                            ht_id=last_match.ht_id,
                            datetime=last_match.datetime.datetime,
                        )

                        match_lineup = chpp.match_lineup(
                            ht_id=match.ht_id,
                            team_id=team.ht_id,
                        )

                        # Calculate match duration
                        m = chpp.match(ht_id=match.ht_id)

                        # match duration determined according
                        # to start time and end time,
                        # majored with additional minutes
                        match_duration = (90 + m.added_minutes
                                          if (m.finished_date - m.datetime
                                              < datetime.timedelta(minutes=120))
                                          else 120 + m.added_minutes
                                          )

                        formations_played = match_lineup.formations

                        for i, (minute,
                                formation,
                                ) in enumerate(formations_played):

                            start_minute = minute

                            # end minute is start_minute
                            # of the following formation
                            # or match_duration if is the last formation played
                            if i < len(formations_played) - 1:
                                end_minute = formations_played[i + 1][0]
                            else:
                                end_minute = match_duration

                            duration = end_minute - start_minute

                            # Save formation
                            # only if duration is not equal to zero
                            if duration > 0:
                                FormationUsePeriod.objects.create(
                                    team=team,
                                    match=match,
                                    formation=formation,
                                    start_minute=start_minute,
                                    end_minute=end_minute,
                                    duration=duration,
                                )

                    # if no match played
                    else:
                        FormationUsePeriod.objects.create(
                            team=team,
                            match=None,
                            formation="",
                            start_minute=0,
                            end_minute=0,
                            duration=0,
                        )

                    # store last update to subscription model
                    subscription.last_update = HTDatetime().datetime
                    subscription.save()

                except (LookupError, ValueError) as e:
                    logger.error(f"Error: {e}")
                    logger.error(f"Team ID : {team.ht_id}")
                    for k, v in locals().items():
                        logger.info(f"{k}: {v}")
